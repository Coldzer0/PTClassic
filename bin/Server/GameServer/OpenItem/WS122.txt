//		아이템 정보		//


*이름		"드래곤 보우"
*Name		"Dragon Bow"
*코드		"WS122"

///////////////	공통사항		 ////////////

*내구력		136	155
*무게		60
*가격		1900000

///////////////	원소		/////////////

*생체		
*대자연		
*불		
*냉기		
*번개		
*독		
*물		
*바람		

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		47 50	68 71
*사정거리	255
*공격속도	8
*명중력		120	150
*크리티컬	17

//////////////	방어성능		/////////////
// 추가요인

*흡수력		
*방어력		
*블럭율		

//////////////	신발성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생	
*근력재생	
*생명력추가	
*기력추가	
*근력추가	
*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		80
*힘		80
*정신력		40
*재능		80
*민첩성		166
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승	
*기력상승	
*근력상승	

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

**특화 Archer
**특화랜덤 Atalanta Mechanician
// Rogue Pilgrim //

**공격력	0	7
**명중력	1	3
**크리티컬	4
**사정거리	20

*연결파일	"name\WS122.zhoon"